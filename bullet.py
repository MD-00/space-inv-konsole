class Bullet:
    def __init__(self, plansza, srodek):
        self.position = srodek.copy()
        self.plansza_width = 25
        self.plansza_height = 10
        plansza[(self.position[0] - 1) * self.plansza_width + self.position[1]] = '|'

    def move(self, plansza):
        if plansza[self.position[0] * self.plansza_width + self.position[1]] != 'x':
            plansza[(self.position[0]) * self.plansza_width + self.position[1]] = ' '
            plansza[(-1 + self.position[0]) * self.plansza_width + self.position[1]] = '|'
        self.position[0] -= 1

    def destroy(self, plansza):
        plansza[(self.position[0]) * self.plansza_width + self.position[1]] = ' '
