from player import Player
import keyboard
from time import sleep
import threading
from plansza import Plansza

plansza_width = 25
plansza_height = 10


def timer_move():
    while True:
        plansza_obj.printuj_plansze(plansza)
        player.update(plansza)
        sleep(0.2)
        plansza_obj.screen_clear()


user_input = ''
plansza_obj = Plansza()
plansza = plansza_obj.tworz_plansze()
player = Player(plansza)
player.update(plansza=plansza)

S = threading.Timer(1, timer_move)
S.start()

while (user_input != 'x'):
    if keyboard.read_key():
        user_input = keyboard.read_key()
        player.move(user_input)
