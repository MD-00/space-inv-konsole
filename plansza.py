import os
from colorama import Fore
from colorama import Style


class Plansza:
    def __init__(self):
        self.plansza_width = 25
        self.plansza_height = 10
        self.plansza = []

    def screen_clear(self):
        os.system("CLS")

    def tworz_plansze(self):
        for i in range(self.plansza_height):
            for j in range(self.plansza_width):
                if j == 0 or j == self.plansza_width - 1:
                    self.plansza.append(('#'))
                else:
                    self.plansza.append(' ')
        return self.plansza

    def printuj_plansze(self, plansza):
        for x in range(len(plansza)):
            if plansza[x] == 'U':
                plansza[x] = f'{Fore.RED}U{Style.RESET_ALL}'
            elif plansza[x] == '|':
                plansza[x] = f'{Fore.YELLOW}|{Style.RESET_ALL}'
            elif plansza[x] == '#':
                plansza[x] = f'{Fore.CYAN}#{Style.RESET_ALL}'
        for i in range(0, self.plansza_width * self.plansza_height, self.plansza_width):
            i += self.plansza_width
            plansza2 = plansza[i - self.plansza_width:i]
            print(''.join(plansza2))
