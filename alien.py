class Alien:
    def __init__(self, position, plansza):
        self.plansza_width = 25
        self.plansza_height = 10
        self.position = position
        self.sign = 'U'
        plansza[self.position[0] * self.plansza_width + self.position[1]] = self.sign

    def move(self, plansza):
        if plansza[(self.position[0] - 1) * self.plansza_width + self.position[1]] == ' ':
            plansza[(self.position[0]) * self.plansza_width + self.position[1]] = ' '
            self.position[0] += 1
            plansza[(self.position[0]) * self.plansza_width + self.position[1]] = self.sign
        else:
            plansza[(self.position[0]) * self.plansza_width + self.position[1]] = self.sign
            self.position[0] += 1
