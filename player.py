from bullet import Bullet
from alien import Alien
from colorama import Fore, Style
from playsound import playsound
import threading

class Player:
    def __init__(self, plansza):
        self.srodek = [9, 12]
        self.plansza = []
        self.plansza = plansza
        self.bullet_list = []
        self.plansza_width = 25
        self.plansza_height = 10
        self.alien_list = []
        self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = 'x'
        self.spawn_aliens(self.plansza)
        self.kierunek = True

    def update(self, plansza):
        self.plansza = plansza
        # print(f'alien len: {len(self.alien_list)}')
        for bullet in self.bullet_list:
            if plansza[
                (bullet.position[0] - 1) * self.plansza_width + bullet.position[1]] == f'{Fore.RED}U{Style.RESET_ALL}':
                for alien in self.alien_list:
                    if alien.position[0] == bullet.position[0] - 1 and alien.position[1] == bullet.position[1]:
                        self.alien_list.remove(alien)
                        T = threading.Thread(target=self.play_sound_death)
                        T.start()
                bullet.move(plansza)
                bullet.destroy(plansza)
                self.bullet_list.remove(bullet)
            elif bullet.position[0] == 1:
                bullet.move(plansza)
                bullet.destroy(plansza)
                self.bullet_list.remove(bullet)
            else:
                bullet.move(plansza)

        # for alien in self.alien_list:
        #     alien.move(plansza)

    def move(self, key):
        if key == 'a':
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = ' '
            self.srodek[1] = self.srodek[1] - 1
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = 'x'

        if key == 'd':
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = ' '
            self.srodek[1] += 1
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = 'x'

        if key == 'w':
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = ' '
            self.srodek[0] -= 1
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = 'x'

        if key == 's':
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = ' '
            self.srodek[0] += 1
            self.plansza[self.srodek[0] * self.plansza_width + self.srodek[1]] = 'x'

        if key == 'c':
            self.shoot(self.plansza)

    def shoot(self, plansza):
        self.bullet_list.append(Bullet(plansza, self.srodek))
        # playsound('shoot.mp3')
        S = threading.Thread(target=self.play_sound_shoot)
        S.start()

    def play_sound_shoot(self):
        playsound('shoot.mp3')

    def play_sound_death(self):
        playsound('death.mp3')

    def spawn_aliens(self, plansza):
        for x in range(5):
            for i in range(7, 18):
                self.alien_list.append(Alien([x, i], plansza))
